package ai.graphen;

import com.linecorp.armeria.client.HttpClient;
import com.linecorp.armeria.common.AggregatedHttpMessage;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ServerTest {
  @BeforeAll
  static void initAll() {
    new Server();
  }

  @Test
  void testServer() {
    HttpClient httpClient = HttpClient.of("http://127.0.0.1:1234/");
    final AggregatedHttpMessage message = httpClient.get("/ping").aggregate().join();

    assertEquals(message.content().toString(), "pong");
  }
}
