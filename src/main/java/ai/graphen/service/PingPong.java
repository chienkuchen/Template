package ai.graphen.service;

import com.linecorp.armeria.common.HttpResponse;
import com.linecorp.armeria.common.HttpStatus;
import com.linecorp.armeria.common.MediaType;
import com.linecorp.armeria.server.annotation.Get;

public class PingPong {
  @Get("/ping")
  public HttpResponse pong() {
    return HttpResponse.of(HttpStatus.OK, MediaType.JSON, "pong");
  }
}
