package ai.graphen;

import ai.graphen.service.PingPong;
import com.linecorp.armeria.common.SessionProtocol;
import com.linecorp.armeria.server.ServerBuilder;
import com.linecorp.armeria.server.logging.LoggingService;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Server {
  public static void main(String[] args) {

    Config conf = ConfigFactory.load();

    ServerBuilder sb;
    sb = new ServerBuilder();
    sb.port(conf.getInt("ServerPort"), SessionProtocol.HTTP);
    sb.annotatedService(new PingPong()).decorator(LoggingService.newDecorator());
    sb.build().start().join();
  }
}
