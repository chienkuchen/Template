# System requirements
* Java 1.8


# Technical stack:
* Asynchronous RPC/REST library built on top of Java 8, Netty, HTTP/2, Thrift and gRPC: [armeria](https://line.github.io/armeria/)

# How do build?
```bash
./gradlew build
```

# How do test?
```bash
./gradlew test
```

# How do build a docker image?
```bash
./gradlew dockerBuildImage
```

# How do run?
```bash
./gradlew run
```
